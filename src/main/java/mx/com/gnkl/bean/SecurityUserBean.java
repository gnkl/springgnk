/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.bean;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author jmejia
 */
public class SecurityUserBean extends org.springframework.security.core.userdetails.User{
    private Integer idUsu;
    public SecurityUserBean(String username, String password, Collection<? extends GrantedAuthority> authorities, Integer idusu) {
        super(username, password, authorities);
        this.idUsu=idusu;
    }

    /**
     * @return the idUsu
     */
    public Integer getIdUsu() {
        return idUsu;
    }

    /**
     * @param idUsu the idUsu to set
     */
    public void setIdUsu(Integer idUsu) {
        this.idUsu = idUsu;
    }
    
}
