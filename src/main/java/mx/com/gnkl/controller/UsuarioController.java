package mx.com.gnkl.controller;

import java.util.Locale;
import mx.com.gnkl.bean.SecurityUserBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/protected/usuario")
public class UsuarioController {

    private static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("crudusuarios");
    }    
    
    @RequestMapping(value = "/getloggeduser", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchorigen(Locale locale) {
        SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new ResponseEntity<Object>(user, HttpStatus.OK);
    }      
}