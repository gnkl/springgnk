/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.component;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

/**
 *
 * @author jmejia
 */
@Component
public class CsrfTokenAsCookieFilter extends OncePerRequestFilter {
  
  public static final String ANGULAR_COOKIE_NAME = "XSRF-TOKEN";
  public static final String ANGULAR_HEADER_NAME = "X-XSRF-TOKEN";
  
  public static final String SPRING_COOKIE_NAME = "CSRF-TOKEN";
  public static final String SPRING_HEADER_NAME = "X-CSRF-TOKEN";
 
  private String cookieName = ANGULAR_COOKIE_NAME;
 
  public String getCookieName() {
    return cookieName;
  }
 
  public void setCookieName(String cookieName) {
    this.cookieName = cookieName;
  }
 
  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
 
    CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
    if (csrf != null) {
      Cookie cookie = WebUtils.getCookie(request, cookieName);
      String token = csrf.getToken();
      if (cookie == null || token != null && !token.equals(cookie.getValue())) {
        cookie = new Cookie(cookieName, token);
        cookie.setPath(request.getServletContext().getContextPath());
        response.addCookie(cookie);
      }
    }
    filterChain.doFilter(request, response);
  }

}