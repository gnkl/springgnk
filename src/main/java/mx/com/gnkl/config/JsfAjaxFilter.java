/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.GenericFilterBean;
/**
 *
 * @author jmejia
 */
public class JsfAjaxFilter extends GenericFilterBean {
   private Logger logger = LoggerFactory.getLogger(getClass());

    private static final String FACES_REQUEST_HEADER = "faces-request";

    private String invalidSessionUrl;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        boolean ajaxRequestReceived = false;

        boolean sessionExpired = false;

        HttpServletRequest httpRequest = null;
        if (request instanceof HttpServletRequest) {
            httpRequest = (HttpServletRequest) request;
            ajaxRequestReceived = "partial/ajax".equals(httpRequest
                    .getHeader(FACES_REQUEST_HEADER));
            sessionExpired = hasSessionExpired(httpRequest);
        }

        if (sessionExpired && ajaxRequestReceived) {
            performRedirect(httpRequest, response);
        } else {
            chain.doFilter(request, response);
        }

    }

    private boolean hasSessionExpired(HttpServletRequest httpRequest) {
        if (httpRequest.getRequestedSessionId() != null
                && !httpRequest.isRequestedSessionIdValid()) {
            if (logger.isDebugEnabled()) {
                logger.debug("Requested session ID "
                        + httpRequest.getRequestedSessionId() + " is invalid.");
            }

            return true;
        }
        return false;
    }

    private void performRedirect(HttpServletRequest request,
            ServletResponse response) throws IOException {
        String contextPath = request.getContextPath();
        String redirectUrl = contextPath + invalidSessionUrl;
        logger.debug(
                "Session expired during ajax request, redirecting to '{}'",
                redirectUrl);

        String ajaxRedirectXml = createAjaxRedirectXml(redirectUrl);
        logger.debug("Ajax partial response to redirect: {}", ajaxRedirectXml);

        response.setContentType("text/xml");
        response.getWriter().write(ajaxRedirectXml);
    }

    private String createAjaxRedirectXml(String redirectUrl) {
        return new StringBuilder()
                .append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
                .append("<partial-response><redirect url=\"")
                .append(redirectUrl)
                .append("\"></redirect></partial-response>").toString();
    }

    public void setInvalidSessionUrl(String invalidSessionUrl) {
        this.invalidSessionUrl = invalidSessionUrl;
    }
    
}
