package mx.com.gnkl.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;
import mx.com.gnk.model.TbRoles;
import mx.com.gnk.model.TbUsuarios;
import mx.com.gnkl.bean.SecurityUserBean;
import mx.com.gnkl.repository.jpa.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private UserRepository userRepository;  
    
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        TbUsuarios user = userRepository.findDataByUsuario(username);
        if(user!=null){
            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
            for (TbRoles role : user.getRoles()){
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getDesRol()));
            }
            return new SecurityUserBean(user.getUsuario(), user.getPass(), grantedAuthorities, user.getIdUsu());            
        }else{
            return new SecurityUserBean(null, null, null, null);
        }
    }
}
