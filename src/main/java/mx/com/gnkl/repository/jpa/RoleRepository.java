/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.repository.jpa;

import java.util.List;
import mx.com.gnk.model.TbRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


/**
 *
 * @author jmejia
 */
public interface RoleRepository extends JpaRepository<TbRoles, Integer>{
    
    @Query(value="SELECT * FROM tb_roles WHERE des_rol NOT IN(:rolename)",nativeQuery = true)
    List<TbRoles> getAllRolesExcept(@Param("rolename") String rolename);
}
