package mx.com.gnkl.repository.jpa;

import java.util.List;
import mx.com.gnk.model.TbUsuarios;
//import mx.com.gnkl.novartis.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<TbUsuarios, Integer> {
    
    @Query("SELECT t FROM TbUsuarios t WHERE t.usuario = :usuario")
    TbUsuarios findDataByUsuario(@Param("usuario") String usuario);   
        
}
