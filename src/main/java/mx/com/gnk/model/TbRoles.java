/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnk.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_roles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbRoles.findAll", query = "SELECT t FROM TbRoles t"),
    @NamedQuery(name = "TbRoles.findByIdRol", query = "SELECT t FROM TbRoles t WHERE t.idRol = :idRol"),
    @NamedQuery(name = "TbRoles.findByDesRol", query = "SELECT t FROM TbRoles t WHERE t.desRol = :desRol")})
public class TbRoles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_rol")
    private Integer idRol;
    @Size(max = 50)
    @Column(name = "des_rol")
    private String desRol;
    
//    @ManyToMany(mappedBy = "roles")
//    private Set<TbUsuarios> usuarios;

    public TbRoles() {
    }

    public TbRoles(Integer idRol) {
        this.idRol = idRol;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getDesRol() {
        return desRol;
    }

    public void setDesRol(String desRol) {
        this.desRol = desRol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRol != null ? idRol.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbRoles)) {
            return false;
        }
        TbRoles other = (TbRoles) object;
        if ((this.idRol == null && other.idRol != null) || (this.idRol != null && !this.idRol.equals(other.idRol))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbRoles[ idRol=" + idRol + " ]";
    }

//    /**
//     * @return the usuarios
//     */
//    public Set<TbUsuarios> getUsuarios() {
//        return usuarios;
//    }
//
//    /**
//     * @param usuarios the usuarios to set
//     */
//    public void setUsuarios(Set<TbUsuarios> usuarios) {
//        this.usuarios = usuarios;
//    }
    
}
