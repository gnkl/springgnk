/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnk.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbUsuarios.findAll", query = "SELECT t FROM TbUsuarios t"),
    @NamedQuery(name = "TbUsuarios.findByIdUsu", query = "SELECT t FROM TbUsuarios t WHERE t.idUsu = :idUsu"),
    @NamedQuery(name = "TbUsuarios.findByNombre", query = "SELECT t FROM TbUsuarios t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "TbUsuarios.findByUsuario", query = "SELECT t FROM TbUsuarios t WHERE t.usuario = :usuario")})
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS, value="session")
public class TbUsuarios implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usu")
    private Integer idUsu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "usuario")
    private String usuario;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "tb_usuario_rol", joinColumns = { 
                    @JoinColumn(name = "id_usu", nullable = false, updatable = false) }, 
                    inverseJoinColumns = { @JoinColumn(name = "id_rol", 
                                    nullable = false, updatable = false) })   
    private Set<TbRoles> roles;
    
    @JsonIgnore
    @Basic(optional = false)
    @NotNull
    @Column(name = "pass")
    private String pass;
    
    @Basic(optional = true)
    @Size(min = 1, max = 255)
    @Column(name = "apellido_pat")
    private String apellidoPat;
    
    @Basic(optional = true)
    @Size(min = 1, max = 255)
    @Column(name = "apellido_mat")
    private String apellidoMat;
    
    @Basic(optional = true)
    @Size(min = 1, max = 255)
    @Column(name = "telefono")
    private String telefono;
    
    @Basic(optional = true)
    @Size(max = 65535)
    @Column(name = "foto")
    private String foto;
    
    @Basic(optional = true)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "status")
    private String status;
    
      

    public TbUsuarios() {
    }

    public TbUsuarios(Integer idUsu) {
        this.idUsu = idUsu;
    }

    public TbUsuarios(Integer idUsu, String nombre, String usuario, String pass) {
        this.idUsu = idUsu;
        this.nombre = nombre;
        this.usuario = usuario;
        this.pass = pass;
    }

    public Integer getIdUsu() {
        return idUsu;
    }

    public void setIdUsu(Integer idUsu) {
        this.idUsu = idUsu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsu != null ? idUsu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbUsuarios)) {
            return false;
        }
        TbUsuarios other = (TbUsuarios) object;
        if ((this.idUsu == null && other.idUsu != null) || (this.idUsu != null && !this.idUsu.equals(other.idUsu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbUsuarios[ idUsu=" + idUsu + " ]";
    }

    /**
     * @return the roles
     */
    public Set<TbRoles> getRoles() {
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(Set<TbRoles> roles) {
        this.roles = roles;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
//
//    @XmlTransient
//    public Set<TbPaquetes> getTbPaquetesSet() {
//        return tbPaquetesSet;
//    }
//
//    public void setTbPaquetesSet(Set<TbPaquetes> tbPaquetesSet) {
//        this.tbPaquetesSet = tbPaquetesSet;
//    }

    /**
     * @return the apellidoPat
     */
    public String getApellidoPat() {
        return apellidoPat;
    }

    /**
     * @param apellidoPat the apellidoPat to set
     */
    public void setApellidoPat(String apellidoPat) {
        this.apellidoPat = apellidoPat;
    }

    /**
     * @return the apellidoMat
     */
    public String getApellidoMat() {
        return apellidoMat;
    }

    /**
     * @param apellidoMat the apellidoMat to set
     */
    public void setApellidoMat(String apellidoMat) {
        this.apellidoMat = apellidoMat;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the foto
     */
    public String getFoto() {
        return foto;
    }

    /**
     * @param foto the foto to set
     */
    public void setFoto(String foto) {
        this.foto = foto;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
}
