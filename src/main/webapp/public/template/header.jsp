<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>


<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div class="masthead" ng-controller="HeaderCtrl" id="principalheader">
    <h3 class="muted">
        <spring:message code='header.message'/>
    </h3>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">GNKL</a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;{{username}}</a></li>
                    <li><a href="<c:url value="/logout" />"><span class="glyphicon glyphicon-log-out"></span></a></li>
                </ul>         
            </div>        
        </nav>
</div>
<script src="<c:url value='/resources/js/pages/header.js' />"></script>