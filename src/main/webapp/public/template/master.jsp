<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!doctype html>
<!--<html lang="pt-BR" id="ng-app" ng-app="app">-->
<html lang="pt-BR" id="ng-app">
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title><spring:message  code="project.title" /></title>
        
        <!-- styles-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
        <link href="<c:url value='/resources/css/project_style.css'  />" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdn.rawgit.com/angular-ui/ui-select/master/dist/select.min.css">
        <link href="<c:url value='/resources/css/sweetalert.css'/>" rel="stylesheet"/>
        <link href="<c:url value='/resources/css/simple.css'/>" rel="stylesheet"/>
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
        
        <!-- js libraries -->
        <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-route.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-touch.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-smart-table/2.1.8/smart-table.js"></script>
        <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.3.3.js"></script>        
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-sanitize.js"></script>
        <script src="<c:url value='/resources/js/pages/select.js'  />"></script>
        <script src="<c:url value='/resources/js/sweetalert.min.js'  />"></script>
        <script src="<c:url value='/resources/js/ngSweetAlert.js'  />"></script>
        <script src="<c:url value='/resources/js/ng-file-upload-all.min.js'  />"></script>
        <script src="<c:url value='/resources/js/ng-file-upload-shim.min.js'  />"></script>
        <script src="https://d3js.org/d3.v3.min.js" charset="utf-8"></script>
        <script src="https://cdn.rawgit.com/novus/nvd3/v1.8.1/build/nv.d3.min.js" charset="utf-8"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-nvd3/1.0.7/angular-nvd3.min.js"></script>
        <script src="https://code.angularjs.org/1.5.5/i18n/angular-locale_es-mx.js"></script>
        <script src="<c:url value='/resources/js/angular-drag-and-drop-lists.js'/>"></script>
        <script src="<c:url value='/resources/js/checklist-model.js'/>"></script>
        
	<meta name="_csrf" content="${_csrf.token}"/>
	<!-- default header name is X-CSRF-TOKEN -->
	<meta name="_csrf_header" content="${_csrf.headerName}"/>
	<!-- ... -->
                
    </head>
    <body>
        <div class="container">
            <tiles:insertAttribute name="header" />

            <tiles:insertAttribute name="body" />
        </div>
        <tiles:insertAttribute name="footer" />
    </body>
</html>