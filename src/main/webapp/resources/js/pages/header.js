var headerapp = angular.module('headerapp', []);

headerapp.config(function ($httpProvider) {
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 

headerapp.controller('HeaderCtrl', ['$scope', '$http', '$location',function($scope, $http, $location) {
    $scope.username='TEST';
    $scope.url='protected/usuario/getloggeduser';
    if($location.$$absUrl.lastIndexOf('/contacts') > 0){
        $scope.activeURL = 'contacts';
    } else{
        $scope.activeURL = 'home';
    }
    
    var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
    $http.get($scope.url, config)
        .success(function (data) {
            console.log(data);
            $scope.username=data.username;
        })
        .error(function(data, status, headers, config) {
            $scope.username='';
        });      
    
}]);

angular.bootstrap(document.getElementById("principalheader"), ['headerapp']);
